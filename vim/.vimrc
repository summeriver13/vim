"鼠标
"set mouse=ni
"行号
"set number
highlight LineNr cterm=bold ctermfg=white
"字体
set guifont=Consolas

"底部显示模式
set showmode
"底部显示模式
set showcmd

"代码高亮
syntax on
"启用256色
set t_Co=256

"缩进
set autoindent
set tabstop=4
"匹配括号高亮
set showmatch

"编码
set enc=utf-8
set fencs=utf-8,gbk,big5,cp936,gb18030,gb2312,utf-16
set fenc=utf-8